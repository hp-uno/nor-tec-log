# **nor-tec** 73383 Sensor Data Logger

## Hardware

**nor-tec** 73383 Weather Station with remote temperature/humidity sensor

* imported from China by [Schou Company AS, Denmark]
* bought from [Action]
* User manual on [Libble](https://www.libble.eu/nor-tec-model-73383/online-manual-829911/)
* [Schou Company e-paper, see p 16 for the similar 73366 model (landscape)](https://www.epaper.dk/schou/hk/hk-home-appliances-and-weather-stations/)
* Aliexpress has [a very similar looking sensor for Fanju weather stations](https://www.aliexpress.com/item/1005006284350745.html?spm=a2g0o.productlist.main.1.2d32sV5zsV5zjW&algo_pvid=24c0c899-f3af-4306-97f3-63d8ad8c1f0d&utparam-url=scene%3Asearch%7Cquery_from%3A)

[Schou Company AS, Denmark]: https://www.schou.com/en-gb/products/home/electric-appliances/weather-stations/weather-station-wireless-w-adapter-1

[Action]: https://www.action.com/nl-nl/

### Base station

* LCD with backlight that can be set to one of four levels (one is 'off')
* Built-in barometer, thermometer and hygrometer
* Dual display for base station temperature/humidity and remote temperature/humidity data
* Remote display can cycle through all (max 3) remote sensors
* Internal data (including barometer) and external data also have 'trend' indicators
* DCF77 radio time receiver, synchronized at 02:00h at night (LCD background light turns off for 5 minutes)
* Alarm function, capacitive snooze/light sensor button
* 8 buttons on the back to set various functions (backlight, &deg;C/F, set alarm, set date/time if no radio signal is available)
* Additional displays:
  * 'weather forecast' sun/clouds/rain (computed from barometric data)
  * 'comfort level' smiley/neutral/frowny face display (computed from temperature and humidity)
  * radio time, date, day of week
  * moon phase
* Base station operates on on 5V/600mA DC mains power supply or on 2 AA batteries
* Holes for wall-mounting, openable stand for placing on a desk

### Remote sensor

* Remote temperature and humidity sensor, transmits on 433.92MHz
* Sensor has two buttons and a sliding switch inside the battery compartment
  * one button marked **&deg;C/&deg;F** to toggle between display modes
  * one button marked **TX** to transmit a datagram
  * a sliding switch with three positions marked 1 / 2 / 3 to select the sensor address.
* Temperature is measured with a thermistor ('RT' on the PCB, looks a bit like a 1N4148 diode)
* Humidity is measured with a humidity sensitive resistor ('RH' on the PCB, looks like a [HR202 sensor (no shell)](https://www.makerhero.com/img/files/download/HR202L-Datasheet.pdf))
* Sensor has a small 2-line LCD to display
  * temperature (2.1 digits plus &deg;C or &deg;F)
  * relative humidity (2 digits plus '%')
  * channel (CH1, CH2, CH3)
  * transmission (antenna symbol)
* Sensor has a red LED that lights up to indicate transmission
* Sensor operates on two 1.5V AA cells
* No screwdriver needed to open the battery compartment

The 73383 sensor(s) are the real data source for this project. They transmit temperature and humidity data, about every minute.

## Purpose of the project

Read **nor-tec** outdoor temperature and humidity sensor data, display it and log it to an SD card.

### Hardware

Version A (2020):

* Arduino Uno (Genuino Geekcrait clone from [Muco])
* WL101-341 433.92MHz receiver module
* DS1302 RTC module (Three-Wire Interface)
* microSD card module (SPI Interface)
* I2C SSD1306 OLED module 128x64px (turned off a few seconds after datagram reception to save power and reduce RFI)
* Button to turn on the OLED on-demand
* LED (shows it's been turned on manually, not as a result of a received datagram)
* Power draw from 5V USB Powerbank: between 50-70mA

Version B (2021):

* [Arduino Nano (from Amazon AZDelivery)](https://www.amazon.nl/dp/B078SBBST6)
* WL101-341 433.92MHz receiver module
* DS1302 RTC module (Three-Wire Interface)
* microSD card module (SPI Interface)
* I2C LCD 16X2 (I2C LCD adapter module with PCF8574, connected to a 16X2 LCD module with 16 characters/2 lines and backlight)
* Button and LED
* Power draw from 5V USB Powerbank: between 10-20mA

NB:

* The Button will probably get re-assigned to cycle through the sensors - the LCD can stay on with the backlight turned off.
* The LED is not really required, maybe it will be replaced with a second button to turn on the LCD backlight for a few seconds.

Version C (in planning):

* [Hiktech WinnerMicro W806 board HLK-W806-KIT-V1.0](https://nl.aliexpress.com/w/wholesale-W806.html?spm=a2g0o.home.search.0)
* WL101-341 433.92MHz receiver module [Muco]](<https://www.muco.nl/output-communicatie-display/53942-433-tx-rx-comm-set-20-100mtr-1000000003031.html>) [Open electronics](https://www.openelectronics.eu/433Mhz-siustuvo-WL102-341-ir-imtuvo-WL101-341-komplektas)
* [microSD card module (SPI Interface)](https://www.reichelt.nl/nl/en/developer-boards-breakout-board-for-microsd-cards-debo-microsd-2-p266045.html?&trstct=pol_2&nbc=1)
* [DS1302 RTC module (Three-Wire Interface)](https://www.az-delivery.de/en/products/rtc-modul)
* [Joystick button module](https://www.hobbyelectronica.nl/product/joystick-duo-as/?gad_source=1) or a [5-button analog keypad](https://www.reichelt.nl/nl/en/arduino-8211-button-module-5-buttons-ard-button-5-p282672.html?&trstct=pol_6&nbc=1)
* [Heltec 2.13" E-ink Display V2 - black/white/red 212x104 SPI (GitHub)](https://github.com/HelTecAutomation/Heltec-E-Ink)

### 433.92MHz Receiver options

* Using 4-pin 433.92MHz receiver modules (Vcc 5V, Data, Data, GND)
* Receiver needs 5V operating voltage
* DATA output is open collector, needs INPUT_PULLUP on receiver port, but this has the advantage that using it with 3.3V inputs (e.g. ESP32) is no problem
* Tested 433.92MHz receiver modules:
  * XLC-RF-5V: L/C, very cheap and simple, 5 RX/TX pairs were ca. &euro;9 from [AZDelivery] at Amazon
  * WL101-341: crystal superhet receiver module, one RX/TX pair was ca. &euro;10 at [Muco], but this receiver has _way_ better sensitivity
  * H3V4F: print L/C, two RX/TX pairs for ca. &euro;13 (H3V4F/H34A, [PEMENOL] at Amazon, or see at [Raynat]). In my tests, the receiver worked better than the XLC-RF-5V, but slightly worse than WL101-341. (NB: It is specified to work with Vcc 3.3V, an advantage for use with e.g. an ESP32.)

  [AZDelivery]: https://www.amazon.de/dp/B076KPWS7G
  [Muco]:       https://www.muco.nl/output-communicatie-display/53942-433-tx-rx-comm-set-20-100mtr-1000000003031.html
  [PEMENOL]:    https://www.amazon.de/dp/B07PMQZD8S
  [Raynat]:     http://raynat.com/product/en/H3V4F-Remote-control-Receiver-Wireless-Module.html

### Software

* Written by Hagen Patzke starting 2020-01-18.
* Inspired from [Ray Wang (Rayshobby LLC) bit receiver code](http://rayshobby.net/?p=8827).

Latest change (2021): use an Arduino Nano and an I2C LCD instead of a Genuino and an OLED.

## Data transmission format

The sensor transmits (ca.) every 60 seconds 6 packets:

    0000 1111 | 0011 0000 | 0101 1100 | 1110 0111 | 0110 0001
    xxxx xxxx | cccc ub?? | tttt tttt | tttt hhhh | hhhh ??nn

* x: ID // changes on battery switch
* c: 4-bit CRC checksum, for exact processing see [CRC Analysis](./analysis/README.md)
* u: unknown (sometimes '1' after power-on, but not always)
* b: low battery indicator
* t: Temperature // in °F as binary number with one decimal place + 90 °F offset
* h: Humidity // BCD-encoded, each nibble is one digit. Surprise! upper nibble can actually become 0xA to indicate 100%
* n: Channel // Channel number 1..3

### Checksum

The checksum is a 4-bit polynomial checksum, but it is computed out-of-sequence, the last nibble is checksummed at the position of the CRC:

    xxxx xxxx | ??nn ub?? | tttt tttt | tttt hhhh | hhhh

Looks like the CPC polynomial (that is visible) is binary 10011 = x^4 + x^1 + x^0 ([Koopmann] 0x9)

[Koopmann] <https://users.ece.cmu.edu/~koopman/crc/crc4.html>

### Temperature

There are 12 bits (3 nibbles) for the temperature, in 1/10 Fahrenheit, offset by 90F. With 12 bits, we can encode the values from 0 to 4095.

    tempF = tempRaw/10 - 90 = (tempRaw - 900) / 10

Values below 900 are therefore negative temperatures.

The formula to convert the temperature into degrees Celsius is:

    tempC = (tempF - 32) * 5 / 9
    tempC = (tempRaw - ((90 + 32) * 10)) * 5 / 9) / 10 = (tempRaw - 1220) / 18

Usually we like to store 1/10 degrees Celsius in an integer (less storage than a floating-point number, and easier to convert to output digits).

The theoretical temperature range of the temperature data is:

     minF = -90.0F (=  (0- 900) / 10), maxF = 319.5F (=  (4095- 900) / 10)
     minC ~ -67.8C (=  (0-1220) / 18), maxC ~ 159.7C (=  (4095-1220) / 18)

The [Danish importer](http://www.shou.com/) lists (for the outdoor sensor) a range from -20C..+60C.

When I opened the sensor (and the weather station), I noticed that the temperature sensor seems to be a diode. (And on the internet I learned this is a common design.)  
_Addendum 2024-06-13: meanwhile I learned (from building a DIY clock kit) that "Glass Shell Precision NTC Thermistors" like the MF58 temperature-dependent resistors look very similar to a diode. Likely it's one of these, they are inexpensive and accurate._

### Humidity

Humidity is stored in BCD (Binary Coded Decimal) in two nibbles. Our shed is very wet (leaky roof) and this winter it has rained a lot, so the sensor actually reported 100% for some time - the reported humidity data was `A0` (100%).

The indoor receiver displays this as 95%, so this is in line with what's stated in the weather station manual. The [Danish importer](http://www.shou.com/) lists (for the outdoor sensor) a range from 20%rH ~ 95%rH (relative Humidity).

### Channel

The channel number is from 1 to 3 - the remote sensors have a slider switch to select one of these three addresses.

### Frequency 433.92MHz

This is a frequency assigned to Industrial, Scientific and Medical (ISM) devices.
(In the EU, another of these frequencies is 866MHz, in the U.S. its 915MHz.)

> ITU RR, (Footnote 5.280) = In Germany, Austria, Bosnia and Herzegovina, Croatia, North Macedonia, Liechtenstein, Montenegro, Portugal, Serbia, Slovenia and Switzerland, the band 433.05–434.79 MHz (center frequency 433.92 MHz) is designated for ISM applications. Radio communication services of these countries operating within this band must accept harmful interference which may be caused by these applications.
> (Source: [Wikipedia](https://en.wikipedia.org/wiki/ISM_radio_band).)

In Germany, the Radio Amateur services also operate in this band from 430MHz to 440MHz, in the so-called "70cm band".

The ISM devices are usually not really operating on 433.92MHz, but the readio receivers are not that accurate eiter, so it works.

_Instead of 433.92MHz, often the shorter "433MHz" is used. Much more accurate would be "434MHz", because 433.92 is just 184 parts per million lower than 434. But then the ISM band starts (roughly) at 433MHz._
