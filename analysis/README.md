# Nor-tec Sensor 73383 Data Logger CRC Checksum Analzsis

## Data transmission format

- Nor-tec weather station + sensor 73383, imported from China by [Schou Company AS, Denmark].
- Software Defined Radio [rtl_433] decodes this data as 'inFactory'
- German company PEARL sells weather stations and sensors with this brand name. Order number for the temperature/humidity sensor alone is [NX-5817-902]. Listed compatible weather stations are FWS-265/280, FWS-330/350, FWS-686, FWS-740/768 and FWS-800.

[Schou Company AS, Denmark]: https://www.schou.com/en-gb/products/home-kitchen/living/electric-appliances/weather-station-wireless-w-adapter-1
[rtl_433]: https://github.com/merbanan/rtl_433
[NX-5817-902]: https://www.pearl.de/a-NX5817-3041.shtml;jsessionid=o28135BC0E61500A7AB091BEF978E8B13

Observed On-Off-Key (OOK) data pattern:

    preamble            syncPrefix        data...(40 bit)                        syncPostfix
    HHLL HHLL HHLL HHLL HLLLLLLLLLLLLLLLL (HLLLL HLLLLLLLL HLLLL HLLLLLLLL ....) HLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL

Breakdown:

- for the discussion we define '1' as an RX signal, '0' as a pause
- there are four 'preamble' pairs '1'/'0' each with a length of ca. 1000uS (probably to train the AGC of the receiver)
- all other parts - syncPre, syncPost, data0, data1 - have a '1' start pulse of ca. 500uS
- syncPre pulse before dataPtr has a '0' pulse length of ca. 8000uS
- data0 (0-bits) have then a '0' pulse length of ca. 2000uS
- data1 (1-bits) have then a '0' pulse length of ca. 4000uS
- syncPost after dataPtr has a '0' pulse length of ca. 16000uS (in the software we declare a datagram as 'received' at 12ms length)

Without losing more than one datagram, output conversion (formatting and printing) is allowed to take up to

     8ms preamble + (42 * 0.5ms) + 8ms + (40 * 2ms) + 12ms = 129ms  (only 0-bits transmitted)

Maximum receive time (best case) is about

     (42 * 0.5ms) + 8ms + (40 * 4ms) + 12ms = 209ms  (only 1-bits transmitted)

Real times should be somewhere between 120ms and 230ms.

## Info from rtl_433

From [https://github.com/merbanan/rtl_433], file src/devices/infactory.c

inFactory Outdoor sensor transmits data temperature, humidity.
Transmissions also includes an id.

The sensor transmits (ca.) every 60 seconds 6 packets.

    0000 1111 | 0011 0000 | 0101 1100 | 1110 0111 | 0110 0001
    xxxx xxxx | cccc cccc | tttt tttt | tttt hhhh | hhhh ??nn

- x: ID // changes on battery switch
- c: Unknown Checksum (changes on every transmit if the other values are different)
- t: Temperature // in °F as binary number with one decimal place + 90 °F offset
- h: Humidity // BCD-encoded, each nibble is one digit
- n: Channel // Channel number 1 - 3

## Hagen's comments

Looks like the checksum is actually 4 bit only:

    0000 1111 | 0011 0000 | 0101 1100 | 1110 0111 | 0110 0001
    xxxx xxxx | cccc ub?? | tttt tttt | tttt hhhh | hhhh ??nn

- c: unknown, probably 4-bit CRC
- u: unknown (sometimes '1' after power-on, but not always)
- b: low battery indicator
- h: Surprise! upper nibble can actually become 0xA to indicate 100%

NB: Since end of 2020, the rtl_433 inFactory driver also check the CRC4 algorithm.

## Output samples

Serial port output sample (early implementation, with added comments)

    14:39:13.306 -> Nortec receiver (0ms)
    14:39:38.423 -> A1E8626421 - 19.6°C 42%rH ch1 - n:0 d:24915ms  c:14360
    14:39:59.605 -> A1E8626421 - 19.6°C 42%rH ch1 - n:1 d:21168ms  c:14372
    14:41:07.710 -> 32C0626431 - 19.6°C 43%rH ch1 - n:2 d:68072ms  c:14372 (normal interval)
    14:42:10.723 -> 3298626431 - 19.6°C 43%rH ch1 - n:3 d:62919ms  c:14380 (unknown bit change)
    14:42:14.379 -> 3218626431 - 19.6°C 43%rH ch1 - n:4 d:3647ms   c:14320 (unknown bit change)
    14:42:17.660 -> 3298626431 - 19.6°C 43%rH ch1 - n:5 d:3284ms   c:14320 (unknown bit change)
    14:42:45.488 -> 752462C441 - 20.0°C 44%rH ch1 - n:6 d:27802ms  c:14360 (weak battery)
    14:42:51.770 -> 9B90628441 - 19.7°C 44%rH ch1 - n:7 d:6269ms   c:14320
    14:42:57.115 -> BA34631441 - 20.2°C 44%rH ch1 - n:8 d:5352ms   c:14324 (weak battery)
    14:43:02.318 -> 81C062A441 - 19.8°C 44%rH ch1 - n:9 d:5214ms   c:14316
    14:44:11.314 -> 819062B441 - 19.9°C 44%rH ch1 - n:10 d:68934ms c:14416 (normal interval)

SD Card output sample (timestamp, raw hex data, channel number, temperature in &deg;Celsius, humidity in %rH)

    2020-02-02 19:07:29,"9AF059A723",3,11.8,72
    2020-02-02 19:08:11,"AC30667481",1,23.2,48
    2020-02-02 19:08:11,"AC30667481",1,23.2,48
    2020-02-02 19:08:12,"AC30667481",1,23.2,48
    2020-02-02 19:08:40,"4E70564982",2,8.8,98
    2020-02-02 19:08:40,"4E70564982",2,8.8,98
    2020-02-02 19:08:41,"4E70564982",2,8.8,98
    2020-02-02 19:08:46,"9A40598733",3,11.7,73
    2020-02-02 19:08:46,"9A40598733",3,11.7,73
    2020-02-02 19:08:46,"9A40598733",3,11.7,73

SD card output from the same sensor, switched from channel 1 to 3 and back

    2020-02-02 19:09:20,"AC90665481",1,23.1,48
    2020-02-02 19:09:20,"AC90665481",1,23.1,48
    2020-02-02 19:09:23,"ACC8665481",1,23.1,48 (unknown bit change)
    2020-02-02 19:09:23,"ACC8665481",1,23.1,48 (unknown bit change)
    2020-02-02 19:09:23,"ACC8665481",1,23.1,48 (unknown bit change)
    2020-02-02 19:09:25,"AC40665482",2,23.1,48
    2020-02-02 19:09:26,"AC40665482",2,23.1,48
    2020-02-02 19:09:26,"AC40665482",2,23.1,48
    2020-02-02 19:09:28,"ACE0665483",3,23.1,48
    2020-02-02 19:09:28,"ACE0665483",3,23.1,48
    2020-02-02 19:09:29,"ACE0665483",3,23.1,48
    2020-02-02 19:09:32,"AC40665482",2,23.1,48
    2020-02-02 19:09:32,"AC40665482",2,23.1,48
    2020-02-02 19:09:34,"AC90665481",1,23.1,48
    2020-02-02 19:09:34,"AC90665481",1,23.1,48

# CRC Analysis

The article [Reverse-Engineering a CRC Algorithm](https://www.cosc.canterbury.ac.nz/greg.ewing/essays/CRC-Reverse-Engineering.html) describes how to analyze a 16-bit CRC.

One property of CRCs is that a single bit at a given position will influence the CRC always in the same way. XORing two messages where only one data bit changes should always influence the CRC bits in the same way. This property - if true - (a) confirms that we actually do have a CRC, and (b) allows us to check which bits are part of the CRC and which are not.

There are a few parts where we can force individual bit changes:

- the channel code (1,2,3)
- the 'weak battery' indicator bit
- in the ID code (potentially several attempts necessary)

Additional parts with naturally occuring single-bit changes are in the data itself - temperature and humidity usually change linearly.

## Hypothesis 1 - we have a CRC

If we have a CRC, a single-bit change should always change the CRC in the same way. Lets look at a few examples:

    "D13C64E521",1,21.8,52,"unknown-bit and weak battery"
    "D16464E521",1,21.8,52,"weak battery"
    "D18864E521",1,21.8,52,"unknown-bit"

If we XOR the messages with each other, we get:

    D13C64E521    D13C64E521    D16464E521    0058000000
    D16464E521    D18864E521    D18864E521    00B4000000
    ----------    ----------    ----------    ----------
    0058000000    00B4000000    00EC000000    00EC000000
    1-bit change  1-bit change  2-bit change

Looking good so far, let's look at other data:

    "122865A511",1,22.5,51,"unknown-bit"
    "127065A511",1,22.5,51
    "129C65A511",1,22.5,51,"unknown-bit and weak battery"

If we XOR the messages with each other, we get:

    122865A511    122865A511    127065A511    0058000000
    127065A511    129C65A511    129C65A511    00B4000000
    ----------    ----------    ----------    ----------
    0058000000    00B4000000    00EC000000    00EC000000

Still good, let's look at other parts (e.g. the channel bit):

    "AC90665481",1,23.1,48,"Channel 1"
    "AC40665482",2,23.1,48,"Channel 2"
    "ACE0665483",3,23.1,48,"Channel 3"

If we XOR the messages with each other, we get:

    AC90665481    AC90665481    AC40665482    0070000002
    AC40665482    ACE0665483    ACE0665483    00A0000001
    ----------    ----------    ----------    ----------
    __D______3    __7______2    __A______1    __D______3
    2-bit change  1-bit change  1-bit change

Still looking good. This hypothesis has not (yet) been falsified.
(It is not proven true, either - we could still find a counter-example.)

**Hypothesis 1 is TRUE** - meanwhile we have enough data.

## Hypothesis 2 - the eight leading ID bits are not part of the CRC

The CRC is in the third nibble. Assumption is that the CRC is computed only over the temperature/humidity/channel data. Then it should not change if there is a bit change in the ID field.

Turns out there is a counter-example in the recorded data:

    "40B05EB461",1,16.3,46,"receive error (leading bit), only rarely seen"
    "C0B05EB461",1,16.3,46,"looks genuine, lots of identical datagrams"
    "D0F05EB461",1,16.3,46,"looks genuine, lots of identical datagrams"

If we XOR some example messages (with identical data parts) with each other, we get:

    C0B05EB461    C0205EC471    C0A05EE451    C0505F0461
    D0F05EB461    D0605EC471    D0E05EE451    D0105F0461
    ----------    ----------    ----------    ----------
    1040000000    1040000000    1040000000    1040000000

(In the raw data there are _lots_ of records with ID codes C0 and D0, and with the same data. Here I list only four examples.)

This indicates that the ID field is also part of the CRC.

**Hypothesis 2 is FALSE**.

There are more examples of the same data (temperatur/humidity/channel) for different ID codes (examples are not difficult to produce - you only need to take out the battery to get a new ID code).

## Hypothesis 3 - we have a 4-bit CRC

### Brute-force approach

As this is only a 4-bit checksum, it is possible to cheat and 'brute-force' check for the polynomial and the parameters (pre- and post-XOR values). For CRC-4, because `x^4` and `x^0` must be part of the polynomial, there are only 8 meaningful polynomials (10001, 10011, 10101, 10111, 11001, 11011, 11101, 11111). There are 16 possible values for the pre-XOR value, also 16 for the post-XOR value, and there are four possibilities for reversal (nibble bits / message nibble sequence).

    8 x 16 x 16 x 4 = 8192 possibilities

Now this amount is something we can easily compute, e.g. with JavaScript in a browser.

If we find the right combination of algorithm, polynomial and parameters - even if the data contains a faulty datagram - we should be able to verify the majority of data records.

**Unfortunately, this approach did not find the polynomial and the parameters** - in 32 records (start sample size), there were never more than 9 matches (=given CRC and computed CRC are the same).

Other changes could be in the sequence - instead of assuming the CRC field is 0 during computation, maybe it is left out, and the ID added at the end, etc. Tinkering with these also did not producce a program run that verified the majority of sample records.

So we need to do some analysis, and think wider:

- maybe we have a partial 8-bit CRC (only the first or last 4 bits stored)?
- maybe we have a 'folded' 8-bit CRC (first and last 4 bits XORed together)?

### Bit Pattern Analysis

A starting point is to isolate all record combinations where only a single bit changes, and compile a "dictionary of influence" - if we have a CRC, and if each bit of the message reproducibly changes the CRC in a defined and isolated way, then even without knowing the correct algorithm, we can build a list of bit positions with the correct XOR changes, to produce the correct checksum.

Here is a partial dictionary for the changes we already found, from sensor data logs (see nortec-hexdata.js and data-analysis.html):

    Meaning        /- ID code ------------\  /- CRC ---\  PO WB ?0 ?0
    Message bit    00 01 02 03  04 05 06 07  08 09 10 11  12 13 14 15
    CRC influence   ?  ?  ?  4   ?  ?  9  D   0  0  0  0   5  B  ?  ?

    Meaning        /- Temperature (in F + 90) * 10 ----\
    Message bit    16 17 18 19  20 21 22 23  24 25 26 27
    CRC influence   ?  ?  ?  ?   1  9  D  F   E  7  A  5

    Meaning        /- Humidity (BCD) -----\  ?0 ?0 /Ch.\
    Message bit    28 29 30 31  32 33 34 35  36 37 38 39
    CRC influence   ?  ?  ?  3   8  4  2  1   ?  ?  7  A

The CRC cannot influence itself (it is not known beforehand!), so its influence (XOR value) is set to 0. For the unknown-bit (believed to be some sort of power-on, 'PO') and weak-battery (WB) bits, and for the channel number (Ch.) bits we already found some data.

A full dictionary consumes one nibble per message bit, i.e. for our 40-bit message, we need only 20 bytes (and we could save 2 bytes by leaving out the four nibbles for the CRC itself).

Even a partial dictionary can probably teach us more about the CRC algorithm / polynomial and the parameters (maybe we can deduce the algorithm after all).

Backup strategy (if we cannot find the algorithm and parameters): Because the CRC field is so short, instead of a CRC lookup table (nibble 0..F -> CRC delta 0..F), we can use the CRC delta table (message bit 0/1 -> CRC delta 0..F).

**Hypothesis 3 is TRUE**.

## Hypothesis 4 - we can deduct the polynomial

Looks like the CPC polynomial (that is visible) is binary 10011 = x^4 + x^1 + x^0 ([Koopmann] 0x9)

[Koopmann] https://users.ece.cmu.edu/~koopman/crc/crc4.html

    p13 B left  p12 5 = 1011 -> (right-shift) = 0110 (xor) 0101 = (1)0011 => OK
    p12 5 right p13 D = 0101 -> (right-shift) = 0010 (xor) 1011 = 1001(1) => OK

The change sequence p22..27 (D F E 7 A 5) is also plausible wityh this polynomial: 

    D = 1101 -> (right-shift) = 0110 (xor) 1001 = 1111 = F
    F = 1111 -> (right-shift) = 0111 (xor) 1001 = 1110 = E
    E = 1110 -> (right-shift) = 0111                   = 7 (0-bit:no XOR division)
    7 = 0111 -> (right-shift) = 0011 (xor) 1001 = 1010 = A
    A = 1010 -> (right-shift) = 0101                   = 5 (0-bit:no XOR division)

Shifting left also works (then we XOR with (1)0011):

    check if p21 can really be 9 = 1001: 
    from p22 D = 1101 -> ( left-shift) = 1010 (x) 0011 = 1001 9 OK
    from p20 1 = 0001 -> (right-shift) = 0000 (x) 1001 = 1001 9 OK

### The Computed Dictionary

With the polynomial we can compute the missing change delta values:

    Meaning        /- ID code ------------\  /- CRC ---\  PO WB ?0 ?0
    Message bit    00 01 02 03  04 05 06 07  08 09 10 11  12 13 14 15
    CRC influence   ?  ?  ?  4   ?  ?  9  D   0  0  0  0   5  B  ?  ?
    computed        6  3  8      2  1        (F  E  7  A)        C  6

    Meaning        /- Temperature (in F + 90) * 10 ----\
    Message bit    16 17 18 19  20 21 22 23  24 25 26 27
    CRC influence   ?  ?  ?  ?   1  9  D  F   E  7  A  5
    computed        3  8  4  2

    Meaning        /- Humidity (BCD) -----\  ?0 ?0 /Ch.\
    Message bit    28 29 30 31  32 33 34 35  36 37 38 39
    CRC influence   ?  ?  ?  3   8  4  2  1   ?  ?  7  A
    computed        B  C  6                 (?9 ?D ?F ?E)

### Observations

- the channel number bits p38/39 have the "wrong" CRC delta value

- An earlier data set left some garbled data, and it looked like some other bits, e.g. p17, had two different CRC deltas. After another filter/cleanup run, this inconsistency went away. Examination of the data looks like we had bit loss during reception - the datagrams before and after the questionable data are all the same, and they are 'correct'.  
This is something to expect with On-Off-Keying on a busy frequency where all sorts of devices communicate. And it is the reason why it is worth the effort to get the CRC computation right, so we can detect bit (run) errors with a higher probability.

### Possible interpretation

- the channel number bits are computed with the respective nibble at the CRC position (p10/11)
- the CRC is computed over 36 message bits
- this has some advantages:
  - the sensor could pre-compute and re-use the partial CRC for ID code, channel and power-on/battery-weak state
  - there would be no 'gap' for the CRC, and no need for artifical data (e.g. 0) inserted at its position
  - it explains why the earlier 'brute force' attempt failed
  - maybe the channel data was swapped with the CRC to avoid an early '0 0 0 1' bit pattern (e.g. because of the receiver AGC)

### Summary

**Hypothesis 4 is TRUE**.

## Hypothesis 5 - we can now compute CRCs with a delta table

We can make a CRC delta list for each bit position:

    delta  = 6384 219d 0000 5bc6 3842 19df e7a5 bc63 8421 fe7a
    guess  = ---- ---- ---- --?? ---- ---- ---- ---- ---- ??--

- The block of '0000' in the 'delta' row is the (known) position of the CRC.
  They mark bits that are not part of the CRC computation itself.
- In the 'guess' row we have four question marks: they are at bit positions 
  that are always 0 in the log data. For these positions, we 'guess' 
  the CRC delta is homogenous within a given nibble. 
  (Their 'delta' values could also be set to '0' to ignore them.)

Looking at crc-check.html, here the CRC delta computation is applied over a data set of a few hundred recorded datagrams, we can conclude: **Hypothesis 5 is TRUE**.

## Hypothesis 6 - we can now compute CRCs with polynom

1. assume we shift left, the polynom is (1)0011, and we calculate over 36 bits (9 nibbles)
2. for the computation, the channel number nibble is placed at the position of the CRC nibble
3. initial crc_in value is 0x00 (the crc register does not come pre-filled)
4. data is prefixed by 4-bit crc for computation (data is shifted into CRC at the lsbit)

See data-crc.html (where we compute the CRC delta list from the polynome): **Hypothesis 6 is TRUE**.

## Additional notes and implications

With this knowledge we now can also make a receiver that corrects the channel bit influence on the CRC 'out of order', so the CRC could be computed during reception (omitting / storing the received CRC separately), and the value corrected individually when the two channel bits are received.

This is possible, because the final CRC can be regarded as a superposition of individual changes caused by a '1' bit in a specific position in the datagram. Computing the CRC via shift/XOR, via table lookup for nibbles and XOR, or via bit position lookup and XOR gives all the same result, so we are allowed to 'mix' methods.

# Data format, fields, and ranges

## Temperature

There are 12 bits (3 nibbles) for the temperature, in 1/10 Fahrenheit, offset by 90F. With 12 bits, we can encode the values from 0 to 4095.

    tempF = tempRaw/10 - 90 = (tempRaw - 900) / 10

Values below 900 are therefore negative temperatures.

The formula to convert the temperature into degrees Celsius is:

    tempC = (tempF - 32) * 5 / 9
    tempC = (tempRaw - ((90 + 32) * 10)) * 5 / 9) / 10 = (tempRaw - 1220) / 18

Usually we like to store 1/10 degrees Celsius in an integer (less storage than a floating-point number, and easier to convert to output digits).

The theoretical temperature range of the temperature data is:

     minF = -90.0F (=  (0- 900) / 10), maxF = 319.5F (=  (4095- 900) / 10)
     minC ~ -67.8C (=  (0-1220) / 18), maxC ~ 159.7C (=  (4095-1220) / 18)

The [Danish importer](http://www.shou.com/) lists (for the outdoor sensor) a range from -20C..+60C.

When I opened the sensor (and the weather station), I noticed that the temperature sensor seems to be a diode. (And on the internet I learned this is a common design.)

## Humidity

Humidity is stored in BCD (Binary Coded Decimal) in two nibbles. Our shed is very wet (leaky roof) and this winter it has rained a lot, so the sensor actually reported 100% for some time - the reported humidity data was `A0` (100%). The indoor receiver displays this as 95% (this is in line with what's stated in the weather station manual).

The [Danish importer](http://www.shou.com/) lists (for the outdoor sensor) a range from 20%rH ~ 95%rH (relative Humidity).
