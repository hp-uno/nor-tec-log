#!/bin/bash
SRC=/mnt/sf_vmxfer
DST=/home/hp/wrk
echo "Collect all source files and sort them in out0..."
cat ${SRC}/2020????.TXT         | sort    >${DST}/out0-all-records.txt
echo "Filter only unique records in out1..."
cat ${DST}/out0-all-records.txt | uniq    >${DST}/out1-unique.txt
echo "Filter only duplicate records in out2 (to avoid erroneous ones)..."
cat ${DST}/out0-all-records.txt | uniq -d >${DST}/out2-duponly.txt
echo "Cut timestamp field from out2 and write all other fields in out3..."
cat ${DST}/out2-duponly.txt     | cut -d',' -f2,3,4,5  >${DST}/out3-dup-no-ts.txt
echo "Write only hexdata records from out3 into out4..."
cat ${DST}/out3-dup-no-ts.txt   | cut -d',' -f1        >${DST}/out4-reconly.txt
echo "Filter out4 for unique records and write into out5..."
cat ${DST}/out4-reconly.txt     | sort | uniq >${DST}/out5-uniquerecords.txt
echo "Convert out5 into nortec-hexdata.js for include with data-analysis.html..."
echo "hexdata = [" >${DST}/nortec-hexdata.js
cat ${DST}/out5-uniquerecords.txt | sed -re "s/$/,/" >>nortec-hexdata.js
echo "];" >>${DST}/nortec-hexdata.js
echo "done."
