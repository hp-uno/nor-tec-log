/* Log nor-tec outdoor temperature and humidity sensor data received on 433.92MHz.

   Written by Hagen Patzke starting 2020-01-18.
   Update 2021-01-23 change from OLED to I2C LCD.
   Update 2022-02-02 add frost warn function
   Please see README.md for more comments.

   This sketch requires:
   - I2C LCD module
   - SD card module
   - 433.92MHz RX module (on pin3)
   Optional, but strongly recommended to get proper timestamps:
   - RTC module (for timestamps)
   Optional:
   - Wake-up button (between GND and pin2)
   - Buzzer for frost warning (between GND and pin4)
*/

#include <Arduino.h>

#include "options.h"
#include "nortec_rx.h"

#if OPT_SLEEP
#include <avr/sleep.h>
#endif // OPT_SLEEP

// SD Card: SPI, CS=D10, MOSI=D11, MISO=D12, SCK=D13
#include <SPI.h>
#include <SD.h>
const int chipSelect = 10;
bool sd_available = true;

// I2C LCD: I2C, SDA=A4, SCL=A5
#include <LCD_I2C.h>
LCD_I2C lcd(0x27);  // Default address of most PCF8574 modules, change according
#define MILL_WIDTH 16
#define MILL_CHAR  65
byte millcount = 0;

#if OPT_RTC
// RTC DS1302: TWI, CLK=D5, DAT=D6, RST=D7
// Library DS1302 from http://www.RinkyDinkElectronics.com/
#include <DS1302.h>
DS1302 rtc(5, 6, 7);  // RST, DAT, CLK
#endif // OPT_RTC

#if OPT_WAKE
// Button BTN=D2 to GND
// Let OLED wake up when
// (a) the button is pressed (show display with high contrast), or
// (b) a datagram was received (show display with low contrast).
#define WAKE_INTERVAL_MS 6000
long wake_timeout;
bool is_wake = true;
bool wake_button = false;
bool display_on = true;
#endif // OPT_WAKE



#define MAXLEN 60
char line[MAXLEN];  // output buffer
byte lofs = 0;      // line offset
char filename[14] = "datalog.txt";  // file name 8+1+3+nul+safety

#if OPT_WAKE
// handle wakeup status variables
void wakeup(bool fromButton) {
  wake_button = fromButton;
  is_wake = true;
  wake_timeout = millis() + WAKE_INTERVAL_MS;
}

// WAKE Button Interrupt Handler (INT0)
void handle_wake() {
  wakeup(true);
}
#endif // OPT_WAKE


// Hex print raw data into buffer
int sprintHex(char buf[], byte o, byte *rcvPtr) {
  byte c, n, k;
  for (k = 0; k < LEN_DATAGRAM_BYTES; k++) {
    c = rcvPtr[k];
    n = c >> 4;
    buf[o++] = n > 9 ? 55 + n : 48 + n;
    n = c & 0x0f;
    buf[o++] = n > 9 ? 55 + n : 48 + n;
  }
  return o;
}

int sprintStr(char buf[], byte o, char *s) {
  while (*s) {
    buf[o++] = *s;
    s++;
  }
  buf[o] = 0; // make sure we always have a terminated string
  return o;
}

int sprintNum(char buf[], byte o, byte digits, int number) {
  byte r = o + digits;
  char *p = buf + r;
  while (digits--) {
    --p;
    *p = char((number % 10) + 48);
    number = number / 10;
  }
  return r;
}

int sprintTempHumi(char buf[], byte o, byte *p) {
  // make interpreted data
  // byte 0 and 1 contain ID code, CRC and 'weak battery' bit(s)
  // byte rxID = p[0];
  // byte rCRC = p[1] >> 4;
  // byte batt = (p[1] >> 3) & 1;
  int  tempRaw = (p[2] << 4) | (p[3] >> 4);
  byte humi = (p[3] & 15) * 10 + (p[4] >> 4); // BCD, but high nibble may become 10(!)
  byte chan = p[4] & 15; // channel number
  // temperature in 0.1 fahrenheit, offset by 90
  // int tempF10 = (tempRaw - 900); // in dezi-Fahrenheit
  // conversion: Celsius = (Fahrenheit - 32) * 5 / 9
  int tempC10 = (tempRaw - 1220) * 0.555555555; // in dezi-Celsius

  // Channel number (1 .. 3)
  buf[o++] = char((chan % 10) + 48);
  buf[o++] = ',';
  // Temperature
  if (tempC10 < 0) {
    buf[o++] = '-';
    tempC10 = -tempC10;
  }
  if (tempC10 > 999) {
    buf[o++] = char((tempC10 / 1000) + 48);
  }
  if (tempC10 > 99) {
    buf[o++] = char((tempC10 / 100) + 48);
  }
  buf[o++] = char((tempC10 / 10) % 10 + 48);
  buf[o++] = '.';
  buf[o++] = char((tempC10 % 10) + 48);
  buf[o++] = ',';
  // Humidity
  if (humi > 99) { // otherwise 100% will print as ':0'
    buf[o++] = '1';
    humi -= 100;
  }
  o = sprintNum(buf, o, 2, humi);
  return o;
}

int lcdSprintTempHumi(char buf[], byte o, byte *p) {
  int  tempRaw = (p[2] << 4) | (p[3] >> 4);
  byte humi = (p[3] & 15) * 10 + (p[4] >> 4); // BCD, but high nibble may become 10(!)
  byte chan = p[4] & 15; // channel number
  int tempC10 = (tempRaw - 1220) * 0.555555555; // in dezi-Celsius
  // Channel number (1 .. 3)
  buf[o++] = char((chan % 10) + 48);
  buf[o++] = ' ';
  // Temperature
  if (tempC10 < 0) {
    buf[o++] = '-';
    tempC10 = -tempC10;
  }
  if (tempC10 > 999) {
    buf[o++] = char((tempC10 / 1000) + 48);
  } else {
    buf[o++] = ' ';
  }
  if (tempC10 > 99) {
    buf[o++] = char((tempC10 / 100) + 48);
  }
  buf[o++] = char((tempC10 / 10) % 10 + 48);
  buf[o++] = '.';
  buf[o++] = char((tempC10 % 10) + 48);
  buf[o++] = 223; // degree sign, also 39 (apostrophe)
  buf[o++] = 'C';
  buf[o++] = ' ';
  // Humidity
  if (humi > 99) { // otherwise 100% will print as ':0'
    buf[o++] = '1';
    humi -= 100;
  }
  o = sprintNum(buf, o, 2, humi);
  buf[o++] = '%';
  return o;
}


#if OPT_RTC
// Render timestamp as string with configurable dividers.
int sprintTime(char buf[], byte o, Time t, char *dividers = "- :") {
  o = sprintNum(buf, o, 4, t.year);
  buf[o++] = dividers[0];
  o = sprintNum(buf, o, 2, t.mon);
  buf[o++] = dividers[0];
  o = sprintNum(buf, o, 2, t.date);
  buf[o++] = dividers[1];
  o = sprintNum(buf, o, 2, t.hour);
  buf[o++] = dividers[2];
  o = sprintNum(buf, o, 2, t.min);
  buf[o++] = dividers[2];
  o = sprintNum(buf, o, 2, t.sec);
  return o;
}

// Filename in the form YYYYMMDD.txt
int sprintFilename(char buf[], byte o, Time t) {
  o = sprintNum(buf, o, 4, t.year);
  o = sprintNum(buf, o, 2, t.mon);
  o = sprintNum(buf, o, 2, t.date);
  o = sprintStr(buf, o, ".txt");
  buf[o] = 0; // make sure we terminate the filename
  return o;
}
#endif // OPT_RTC


void setup() {
#if OPT_RTC
  rtc.writeProtect(true); // we only read from the RTC
#endif // OPT_RTC
#if OPT_SERIAL
  // Serial setup
  Serial.begin(115200);
  Serial.println("nor-tec logger");
#endif // OPT_SERIAL

  // LCD_I2C setup
  lcd.begin();
  lcd.clear();
  lcd.backlight();
  lcd.print("nor-tec logger");

  sd_available = SD.begin(chipSelect);
  if (!sd_available) {
    lcd.setCursor(0, 1);
    lcd.print("!SD Card Error");
#if OPT_SERIAL
    Serial.println("SD Card Error - No Logging");
#endif // OPT_SERIAL
  }

#if OPT_WAKE
  // OLED display wake-up button
  pinMode(2, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(2), handle_wake, FALLING);
#if OPT_WAKE_LED
  // DEBUG LED on Pin 8
  pinMode(8, OUTPUT);
  digitalWrite(8, LOW);
#endif // OPT_WAKE_LED
#endif // OPT_WAKE

#if OPT_FROSTWARN
  // BUZZER
  pinMode(4, OUTPUT);
#endif // OPT_FROSTWARN

  // 433.92MHz Receiver setup
  pinMode(3, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(3), handle_rx, CHANGE);
}

void loop() {
#if OPT_WAKE
  if (is_wake) {
    if (wake_timeout < millis()) { // timeout expired
      is_wake = false;
      wake_button = false;
#if OPT_WAKE_LED
      digitalWrite(8, LOW);
#endif // OPT_WAKE_LED
      if (display_on) {
        display_on = false;
        lcd.noBacklight();
      }
    } else { // timeout not expired
      if (!display_on) {
        display_on = true;
        lcd.backlight();
#if OPT_WAKE_LED
        if (wake_button) {
          digitalWrite(8, HIGH);
#if OPT_FROSTWARN
          noTone(BUZZ_PIN);
#endif // OPT_FROSTWARN
        }
#endif // OPT_WAKE_LED
      }
    }
  }
#endif // OPT_WAKE

  if (!received) {
#if OPT_SLEEP
    // Tested sleep modes (with RTC):
    // GOOD - SLEEP_MODE_IDLE
    // FAIL - SLEEP_MODE_ADC, SLEEP_MODE_PWR_DOWN, SLEEP_MODE_PWR_SAVE
    // FAIL - SLEEP_MODE_STANDBY, SLEEP_MODE_EXT_STANDBY
    set_sleep_mode(SLEEP_MODE_IDLE);
    sleep_enable();
    sleep_cpu();
    // Execution continues after return from idle with the next line.
    // No need to return if we have got a datagram.
    if (!received) {
      return;
    }
#else
    return;
#endif // OPT_SLEEP
  }

  while (posCurrReceiving != posFirstReceived) {
    lofs = 0; // reset line offset

    byte *rcvPtr = &ring_buffer[posFirstReceived * LEN_DATAGRAM_BYTES];
    bool known = false;

    if (posOurLast != posFirstReceived) {
      byte *lstPtr = &ring_buffer[posOurLast * LEN_DATAGRAM_BYTES];
      byte k;
      known = true;
      for (k = 0; known && k < LEN_DATAGRAM_BYTES; k++) {
        if (lstPtr[k] != rcvPtr[k])
          known = false;
      }
      posOurLast = posFirstReceived;
    }


    if (!known) {

#if OPT_CRC_CHECK
      // Check if the datagram is valid
      bool crc_ok = nortec_crc_ok(rcvPtr);
#if OPT_FROSTWARN
    // 12 bit raw temperature in 0.1 F, offset by 90F (900 == 0)
    // Frost warner - only when CRC is OK, check channel 2 for temp lower than 5 degC
    if (crc_ok) {
      byte chan = rcvPtr[4] & 15; // channel number
      if (chan == FROST_CHAN){
        int  tempRaw = (rcvPtr[2] << 4) | (rcvPtr[3] >> 4);
        if (tempRaw > FROST_TEMP) {
          noTone(BUZZ_PIN);
        } else { // too cold
          tone(BUZZ_PIN, BUZZ_TONE);
        }
      }
    }
#endif // OPT_FROSTWARN
#endif // OPT_CRC_CHECK

      // construct output in buffer
#if OPT_RTC
      Time t = rtc.getTime();
      lofs = sprintTime(line, lofs, t, "- :");
      line[lofs++] = ',';
      if (sd_available) {
        (void)sprintFilename(filename, 0, t);
      }
#endif // OPT_RTC
      line[lofs++] = '"';
      lofs = sprintHex(line, lofs, rcvPtr);
      line[lofs++] = '"';
      line[lofs++] = ',';
      lofs = sprintTempHumi(line, lofs, rcvPtr);
#if OPT_CRC_CHECK
      // document that the CRC was OK (or not)
      line[lofs++] = ',';
      line[lofs++] = crc_ok ? '1' : '0';
#endif // OPT_CRC_CHECK
      line[lofs++] = 0;

#if OPT_SERIAL
      Serial.print(line);
      Serial.flush(); // otherwise we could see later modifications in the output
      Serial.print(" X=");
      Serial.print(loopCount);
      Serial.print(" f=");
      Serial.print(posFirstReceived);
      Serial.print(" l=");
      Serial.print(posOurLast);
#endif // OPT_SERIAL

      if (sd_available) {
        File datalog = SD.open(filename, FILE_WRITE);
        if (datalog) {
          datalog.println(&line[0]);
          datalog.flush();
          datalog.close();
        }
      }

      // LCD output
      lcd.clear();
      // re-use the timestamp for LCD output, but cut off the seconds
      line[16] = 0; //  0..18 timestamp, 20..31 hex received, 33..end = channel,temp,humi
      lcd.setCursor(0, 0);
      lcd.print(&line[0]);
      lofs = lcdSprintTempHumi(line, 33, rcvPtr);
      line[lofs++] = 0;
      lcd.setCursor(0, 1);
      lcd.print(&line[33]);       //  0..18 timestamp

    }// if !known

    // even if we know the data is identical, advance the mill counter
    lcd.setCursor(15, 1);
    lcd.print((char)(MILL_CHAR + millcount));
    millcount = (millcount + 1) % MILL_WIDTH;
#if OPT_WAKE
    wakeup(false); // also wake up the display when we receive something
#endif // OPT_WAKE

    posFirstReceived = (posFirstReceived + 1) % LOG_SIZE;
  } // while

  loopCount++;
  // reset flag
  received = false;
}

//eof
