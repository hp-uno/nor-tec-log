//// NORTEC DECODER

#include <Arduino.h>

#include "options.h"
#include "nortec_rx.h"

#if OPT_SLEEP
#include <avr/sleep.h>
#endif // OPT_SLEEP

// ring buffer for received data
byte ring_buffer[LOG_SIZE * LEN_DATAGRAM_BYTES];
unsigned long timestamp[LOG_SIZE]; // used for printing delta time (serial only)
bool received = false; // main loop shortcut if we have no data
int posLastReceived = 0;
int posCurrReceiving = 1;
int posFirstReceived = 1;
int posOurLast = 1;
int loopCount = 0;

// RX Interrupt Handler (INT1)
void handle_rx() {
  static unsigned long timeLast = 0;
  static long durationLast = 0;
  static byte levelLast = 0;
  static byte posCurBit = 0;

  unsigned long timeNow  = micros();
  byte levelNow = 1 - (digitalRead(DATAPIN) & 1); // level of currently COMPLETED gap/signal
  long durationNow = timeNow - timeLast;

  if ((levelLast == LEVEL_PRE) && (durationLast > LEN_PRE_MIN) && (durationLast < LEN_PRE_MAX)) {
    // we get an interrupt when the signal changes, so now we are processing a signal duration
    // to save computation time, we only check minimal lengths
    // and now we just got a signal pulse in - max 500us time to process stuff
    if (durationNow > LEN_END_MIN) { // end-of-datagram gap
      if (posCurBit == LEN_DATAGRAM_BITS) { // we have all bits
        posLastReceived = posCurrReceiving;
        posCurrReceiving = (posCurrReceiving + 1) % LOG_SIZE;
        if (!received) { // no output running, log start pos of first received datagram
          posFirstReceived = posLastReceived;
          received = true;
#if OPT_SLEEP
          sleep_disable();
#endif // OPT_SLEEP
        }
      }
      posCurBit = 0; // reset bit position within datagram
    } else if (durationNow > LEN_STA_MIN) { // start-of-datagram gap
      posCurBit = 0;
      timestamp[posCurrReceiving] = timeNow;
    } else { // we probably have a data bit
      byte  d = (posCurBit % LEN_DATAGRAM_BITS) >> 3; // make sure we stay within our datagram
      byte *p = ring_buffer + (posCurrReceiving * LEN_DATAGRAM_BYTES) + d;
      byte  m = 0x80 >> (posCurBit & 7); // bitmask
      if (durationNow > LEN_BI1_MIN) { // it is a one
        *p |= m;
        posCurBit++;
      } else if (durationNow > LEN_BI0_MIN) { // it is a zero
        *p &= ~m;
        posCurBit++;
      } else { // error: the pulse we got is too short - lets reset the datagram
        posCurBit = 0;
      }
    } // data bit
  }

  timeLast = timeNow;
  levelLast = levelNow;
  durationLast = durationNow;
} // end handle_rx (INT1)


#if OPT_CRC_CHECK
// CRC check routine
bool nortec_crc_ok(byte *rcvPtr) {
  byte m[LEN_DATAGRAM_BYTES];
  memcpy(m, rcvPtr, LEN_DATAGRAM_BYTES);
  byte msg_crc = m[1] >> 4;
  m[1] = (m[4] << 4) | (m[1] & 0x0F); // for computation, channel info (last nibble) is at CRC position
  byte new_crc = m[0] >> 4; // CRC preload = 0, therefore we can skip these 4 bits
  for (int k = 4; k < 36; k++) {
    new_crc <<= 1;
    if (m[k >> 3] & (0x80 >> (k & 7))) {
      new_crc |= 1;
    }
    if (new_crc & 0x10) {
      new_crc ^= 0x13;
    }
  }
  return (new_crc == msg_crc);
} // end nortec_crc_ok
#endif // OPT_CRC_CHECK