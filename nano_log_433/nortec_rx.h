//// NORTEC DECODER

// RX module for 433.92MHz: DATA=D3
#define DATAPIN      3  // D3 is interrupt 1
#define LOG_SIZE     20

// Nortec datagram is 5 bytes long
#define LEN_DATAGRAM_BITS  40
#define LEN_DATAGRAM_BYTES 5
// observed parameters:
// pre=500uS, bit0=1968uS, bit1=3980uS, start_gap=7976uS, end_gap=15976uS
#define LEVEL_PRE    1     // level of the fixed width prefix impulse
#define LEN_PRE_MIN  400
#define LEN_PRE_MAX  600
#define LEN_BI0_MIN  1250
#define LEN_BI1_MIN  3250
#define LEN_STA_MIN  7000
#define LEN_END_MIN  10000

extern byte ring_buffer[LOG_SIZE * LEN_DATAGRAM_BYTES];
extern bool received;
extern int posCurrReceiving;
extern int posFirstReceived;
extern int posOurLast;
extern int loopCount;

// RX Interrupt Handler (INT1)
extern void handle_rx();

#if OPT_CRC_CHECK
extern bool nortec_crc_ok(byte *rcvPtr);
#endif // OPT_CRC_CHECK
