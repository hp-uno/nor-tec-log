// Program options
#define OPT_SERIAL    0   // Serial output (1=on, 0=off)
#define OPT_RTC       1   // use time from DS1302 RTC module
#define OPT_SLEEP     1   // Select sleep mode
#define OPT_WAKE      1   // Wake-up button support (display power save)
#define OPT_WAKE_LED  1   // Wake-up LED support (on if button press caused wakeup)
#define OPT_CRC_CHECK 1   // CRC check received datagrams
#define OPT_FROSTWARN 1   // CRC check received datagrams

#if OPT_FROSTWARN
// TEST
// #define FROST_CHAN 1
// #define FROST_TEMP 1633 // 22.9 degC = 73.3 degF, 900 + 730 = 1633
// LIVE
#define FROST_CHAN 2
#define FROST_TEMP 1310 // 5.0 degC = 41.0 degF, anything below is too cold = 900 + 410 = 1310
#define BUZZ_PIN   4
#define BUZZ_TONE  1000
#endif
