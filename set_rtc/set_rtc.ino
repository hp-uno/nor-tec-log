// RTC DS1302: TWI, CLK=D7, DAT=D6, RST=D5
// Library DS1302 from http://www.RinkyDinkElectronics.com/
#include <DS1302.h>
DS1302 rtc(5, 6, 7);  // RST, DAT, CLK

void setup() {
  rtc.writeProtect(false);
  rtc.halt(false);
  rtc.setDOW(SATURDAY);
  rtc.setTime(21, 45, 00);
  rtc.setDate(23, 01, 2021);
  rtc.writeProtect(true); // from now we only read from the RTC
}

void loop() {
  // no-op
}
