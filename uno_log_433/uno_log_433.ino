/* Log nor-tec outdoor temperature and humidity sensor data received on 433.92MHz.

   Written by Hagen Patzke starting 2020-01-18.
   Please see README.md for more comments.

   This sketch requires:
   - SSD1306 OLED module
   - SD card module
   - 433.92MHz RX module
   Optional, but strongly recommended to get proper timestamps:
   - RTC module (for timestamps)
   Optional:
   - Wake-up button (between GND and pin2)
*/

#define OPT_SERIAL   0   // Serial output (1=on, 0=off)
#define OPT_RTC      1   // use time from DS1302 RTC module
#define OPT_SLEEP    1   // Select sleep mode
#define OPT_WAKE     1   // Wake-up button support (display power save)
#define OPT_WAKE_LED 1   // Wake-up LED support (on if button press caused wakeup)

#include <Arduino.h>
#if OPT_SLEEP
#include <avr/sleep.h>
#endif // OPT_SLEEP

// SD Card: SPI, CS=D10, MOSI=D11, MISO=D12, SCK=D13
#include <SPI.h>
#include <SD.h>
const int chipSelect = 10;
bool sd_available = true;

// OLED SSD1306: I2C, SDA=A4, SCL=A5
#include <U8x8lib.h>
#include <Wire.h>
U8X8_SSD1306_128X64_NONAME_HW_I2C u8x8(/* reset=*/ U8X8_PIN_NONE);
#define MILL_WIDTH 16
#define MILL_CHAR 46
byte millcount = 0;

#if OPT_RTC
// RTC DS1302: TWI, CLK=D5, DAT=D6, RST=D7
// Library DS1302 from http://www.RinkyDinkElectronics.com/
#include <DS1302.h>
DS1302 rtc(7, 6, 5);  // RST, DAT, CLK
#endif // OPT_RTC

#if OPT_WAKE
// Button BTN=D2 to GND
// Let OLED wake up when
// (a) the button is pressed (show display with high contrast), or
// (b) a datagram was received (show display with low contrast).
#define WAKE_INTERVAL_MS 6000
long wake_timeout;
bool is_wake = true;
bool wake_button = false;
bool display_on = true;
#endif // OPT_WAKE


//// NORTEC DECODER

// RX module for 433.92MHz: DATA=D3
#define DATAPIN      3  // D3 is interrupt 1
#define LOG_SIZE     20

// Nortec datagram is 5 bytes long
#define LEN_DATAGRAM_BITS  40
#define LEN_DATAGRAM_BYTES 5
// observed parameters:
// pre=500uS, bit0=1968uS, bit1=3980uS, start_gap=7976uS, end_gap=15976uS
#define LEVEL_PRE    1     // level of the fixed width prefix impulse
#define LEN_PRE_MIN  400
#define LEN_PRE_MAX  600
#define LEN_BI0_MIN  1250
#define LEN_BI1_MIN  3250
#define LEN_STA_MIN  7000
#define LEN_END_MIN  10000

// ring buffer for received data
byte ring_buffer[LOG_SIZE * LEN_DATAGRAM_BYTES];
unsigned long timestamp[LOG_SIZE]; // used for printing delta time (serial only)
bool received = false; // main loop shortcut if we have no data
int posLastReceived = 0;
int posCurrReceiving = 1;
int posFirstReceived = 1;
int posOurLast = 1;
int loopCount = 0;

#define MAXLEN 60
char line[MAXLEN];  // output buffer
byte lofs = 0;      // line offset
char filename[14] = "datalog.txt";  // file name 8+1+3+nul+safety

#if OPT_WAKE
// handle wakeup status variables
void wakeup(bool fromButton) {
  wake_button = fromButton;
  is_wake = true;
  wake_timeout = millis() + WAKE_INTERVAL_MS;
}

// WAKE Button Interrupt Handler (INT0)
void handle_wake() {
  wakeup(true);
}
#endif // OPT_WAKE

// RX Interrupt Handler (INT1)
void handle_rx() {
  static unsigned long timeLast = 0;
  static long durationLast = 0;
  static byte levelLast = 0;
  static byte posCurBit = 0;

  unsigned long timeNow  = micros();
  byte levelNow = 1 - (digitalRead(DATAPIN) & 1); // level of currently COMPLETED gap/signal
  long durationNow = timeNow - timeLast;

  if ((levelLast == LEVEL_PRE) && (durationLast > LEN_PRE_MIN) && (durationLast < LEN_PRE_MAX)) {
    // we get an interrupt when the signal changes, so now we are processing a signal duration
    // to save computation time, we only check minimal lengths
    // and now we just got a signal pulse in - max 500us time to process stuff
    if (durationNow > LEN_END_MIN) { // end-of-datagram gap
      if (posCurBit == LEN_DATAGRAM_BITS) { // we have all bits
        posLastReceived = posCurrReceiving;
        posCurrReceiving = (posCurrReceiving + 1) % LOG_SIZE;
        if (!received) { // no output running, log start pos of first received datagram
          posFirstReceived = posLastReceived;
          received = true;
#if OPT_SLEEP
          sleep_disable();
#endif // OPT_SLEEP
        }
      }
      posCurBit = 0; // reset bit position within datagram
    } else if (durationNow > LEN_STA_MIN) { // start-of-datagram gap
      posCurBit = 0;
      timestamp[posCurrReceiving] = timeNow;
    } else { // we probably have a data bit
      byte  d = (posCurBit % LEN_DATAGRAM_BITS) >> 3; // make sure we stay within our datagram
      byte *p = ring_buffer + (posCurrReceiving * LEN_DATAGRAM_BYTES) + d;
      byte  m = 0x80 >> (posCurBit & 7); // bitmask
      if (durationNow > LEN_BI1_MIN) { // it is a one
        *p |= m;
        posCurBit++;
      } else if (durationNow > LEN_BI0_MIN) { // it is a zero
        *p &= ~m;
        posCurBit++;
      } else { // error: the pulse we got is too short - lets reset the datagram
        posCurBit = 0;
      }
    } // data bit
  }

  timeLast = timeNow;
  levelLast = levelNow;
  durationLast = durationNow;
} // end handle_rx (INT1)

// Hex print raw data into buffer
int sprintHex(char buf[], byte o, byte *rcvPtr) {
  byte c, n, k;
  for (k = 0; k < LEN_DATAGRAM_BYTES; k++) {
    c = rcvPtr[k];
    n = c >> 4;
    buf[o++] = n > 9 ? 55 + n : 48 + n;
    n = c & 0x0f;
    buf[o++] = n > 9 ? 55 + n : 48 + n;
  }
  return o;
}

int sprintStr(char buf[], byte o, char *s) {
  while (*s) {
    buf[o++] = *s;
    s++;
  }
  buf[o] = 0; // make sure we always have a terminated string
  return o;
}

int sprintNum(char buf[], byte o, byte digits, int number) {
  byte r = o + digits;
  char *p = buf + r;
  while (digits--) {
    --p;
    *p = char((number % 10) + 48);
    number = number / 10;
  }
  return r;
}

int sprintTempHumi(char buf[], byte o, byte *p) {
  // make interpreted data
  // byte 0 and 1 contain ID code, CRC and 'weak battery' bit(s)
  // byte rxID = p[0];
  // byte rCRC = p[1] >> 4;
  // byte batt = (p[1] >> 3) & 1;
  int  tempRaw = (p[2] << 4) | (p[3] >> 4);
  byte humi = (p[3] & 15) * 10 + (p[4] >> 4); // BCD, but high nibble may become 10(!)
  byte chan = p[4] & 15; // channel number
  // temperature in 0.1 fahrenheit, offset by 90
  // int tempF10 = (tempRaw - 900); // in dezi-Fahrenheit
  // conversion: Celsius = (Fahrenheit - 32) * 5 / 9
  int tempC10 = (tempRaw - 1220) * 0.555555555; // in dezi-Celsius

  // Channel number (1 .. 3)
  buf[o++] = char((chan % 10) + 48);
  buf[o++] = ',';
  // Temperature
  if (tempC10 < 0) {
    buf[o++] = '-';
    tempC10 = -tempC10;
  }
  if (tempC10 > 999) {
    buf[o++] = char((tempC10 / 1000) + 48);
  }
  if (tempC10 > 99) {
    buf[o++] = char((tempC10 / 100) + 48);
  }
  buf[o++] = char((tempC10 / 10) % 10 + 48);
  buf[o++] = '.';
  buf[o++] = char((tempC10 % 10) + 48);
  buf[o++] = ',';
  // Humidity
  if (humi > 99) { // otherwise 100% will print as ':0'
    buf[o++] = '1';
    humi -= 100;
  }
  o = sprintNum(buf, o, 2, humi);
  return o;
}

#if OPT_RTC
// Render timestamp as string with configurable dividers.
int sprintTime(char buf[], byte o, Time t, char *dividers = "- :") {
  o = sprintNum(buf, o, 4, t.year);
  buf[o++] = dividers[0];
  o = sprintNum(buf, o, 2, t.mon);
  buf[o++] = dividers[0];
  o = sprintNum(buf, o, 2, t.date);
  buf[o++] = dividers[1];
  o = sprintNum(buf, o, 2, t.hour);
  buf[o++] = dividers[2];
  o = sprintNum(buf, o, 2, t.min);
  buf[o++] = dividers[2];
  o = sprintNum(buf, o, 2, t.sec);
  return o;
}

// Filename in the form YYYYMMDD.txt
int sprintFilename(char buf[], byte o, Time t) {
  o = sprintNum(buf, o, 4, t.year);
  o = sprintNum(buf, o, 2, t.mon);
  o = sprintNum(buf, o, 2, t.date);
  o = sprintStr(buf, o, ".txt");
  buf[o] = 0; // make sure we terminate the filename
  return o;
}
#endif // OPT_RTC


void setup() {
#if OPT_RTC
  //  rtc.writeProtect(false);
  //  rtc.halt(false);
  //  rtc.setDOW(SATURDAY);
  //  rtc.setTime(12, 15, 45);
  //  rtc.setDate(18, 01, 2020);
  rtc.writeProtect(true); // we only read from the RTC
#endif // OPT_RTC
#if OPT_SERIAL
  // Serial setup
  Serial.begin(115200);
  Serial.println("nor-tec logger");
#endif // OPT_SERIAL

  // OLED setup
  u8x8.begin();
  u8x8.setFont(u8x8_font_amstrad_cpc_extended_r);
  u8x8.clear();
  u8x8.setContrast(64);
  u8x8.draw1x2String(0, 0, "nor-tec logger");

  sd_available = SD.begin(chipSelect);
  if (!sd_available) {
    u8x8.setCursor(0, 2);
    u8x8.println("!SD Card Error");
#if OPT_SERIAL
    Serial.println("SD Card Error - No Logging");
#endif // OPT_SERIAL
  }

#if OPT_WAKE
  // OLED display wake-up button
  pinMode(2, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(2), handle_wake, FALLING);
#if OPT_WAKE_LED
  // DEBUG LED on Pin 8
  pinMode(8, OUTPUT);
  digitalWrite(8, LOW);
#endif // OPT_WAKE_LED
#endif // OPT_WAKE

  // 433.92MHz Receiver setup
  pinMode(3, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(3), handle_rx, CHANGE);
}

void loop() {
#if OPT_WAKE
  if (is_wake) {
    if (wake_timeout < millis()) { // timeout expired
      is_wake = false;
      wake_button = false;
#if OPT_WAKE_LED
      digitalWrite(8, LOW);
#endif // OPT_WAKE_LED
      if (display_on) {
        display_on = false;
        u8x8.setPowerSave(1);
      }
    } else { // timeout not expired
      if (!display_on) {
        display_on = true;
        u8x8.setContrast(wake_button ? 255 : 16);
        u8x8.setPowerSave(0);
#if OPT_WAKE_LED
        if (wake_button) {
          digitalWrite(8, HIGH);
        }
#endif // OPT_WAKE_LED
      }
    }
  }
#endif // OPT_WAKE

  if (!received) {
#if OPT_SLEEP
    // Tested sleep modes (with RTC):
    // GOOD - SLEEP_MODE_IDLE
    // FAIL - SLEEP_MODE_ADC, SLEEP_MODE_PWR_DOWN, SLEEP_MODE_PWR_SAVE
    // FAIL - SLEEP_MODE_STANDBY, SLEEP_MODE_EXT_STANDBY
    set_sleep_mode(SLEEP_MODE_IDLE);
    sleep_enable();
    sleep_cpu();
    // Execution continues after return from idle with the next line.
    // No need to return if we have got a datagram.
    if (!received) {
      return;
    }
#else
    return;
#endif // OPT_SLEEP
  }

  while (posCurrReceiving != posFirstReceived) {
    lofs = 0; // reset line offset

    byte *rcvPtr = &ring_buffer[posFirstReceived * LEN_DATAGRAM_BYTES];
    bool known = false;

    if (posOurLast != posFirstReceived) {
      byte *lstPtr = &ring_buffer[posOurLast * LEN_DATAGRAM_BYTES];
      byte k;
      known = true;
      for (k = 0; known && k < LEN_DATAGRAM_BYTES; k++) {
        if (lstPtr[k] != rcvPtr[k])
          known = false;
      }
      posOurLast = posFirstReceived;
    }

    if (!known) {
      // construct output in buffer
#if OPT_RTC
      Time t = rtc.getTime();
      lofs = sprintTime(line, lofs, t, "- :");
      line[lofs++] = ',';
      if (sd_available) {
        (void)sprintFilename(filename, 0, t);
      }
#endif // OPT_RTC
      line[lofs++] = '"';
      lofs = sprintHex(line, lofs, rcvPtr);
      line[lofs++] = '"';
      line[lofs++] = ',';
      lofs = sprintTempHumi(line, lofs, rcvPtr);
      line[lofs++] = 0;

#if OPT_SERIAL
      Serial.print(line);
      Serial.flush(); // otherwise we could see later modifications in the output
      Serial.print(" X=");
      Serial.print(loopCount);
      Serial.print(" f=");
      Serial.print(posFirstReceived);
      Serial.print(" l=");
      Serial.print(posOurLast);
      Serial.print(" d=");
      Serial.println(timestamp[posFirstReceived] - ((posFirstReceived > 0) ? timestamp[posFirstReceived - 1] : 0));
#endif // OPT_SERIAL

      if (sd_available) {
        File datalog = SD.open(filename, FILE_WRITE);
        if (datalog) {
          datalog.println(&line[0]);
          datalog.flush();
          datalog.close();
        }
      }

      // chop up output buffer for OLED output
      u8x8.setCursor(0, 3);
      line[10] = 0;
      u8x8.println(line);
      line[19] = 0;
      u8x8.println(line + 11);
      line[31] = 0;
      u8x8.println(line + 21);
      u8x8.println(line + 33);
    }// if !known

    // even if we know the data is identical, advance the mill counter
    u8x8.drawGlyph(millcount, 7, 32);
    millcount = (millcount + 1) % MILL_WIDTH;
    u8x8.drawGlyph(millcount, 7, MILL_CHAR);
    u8x8.flush();
    wakeup(false); // also wake up the display when we receive something

    posFirstReceived = (posFirstReceived + 1) % LOG_SIZE;
  } // while

  loopCount++;
  // reset flag
  received = false;
}

//eof
